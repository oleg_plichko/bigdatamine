from django.db import models

# Create your models here.
from work.models import BigData


class Country(models.Model):
    name = models.CharField(primary_key=True, max_length=140)

    def __unicode__(self):
        return self.name


class City(models.Model):
    name = models.CharField(primary_key=True, max_length=140)
    country = models.ForeignKey(Country)

    def __unicode__(self):
        return '%s(%s)' % (self.name, self.country)


class BigDataItem(models.Model):
    bigdata = models.ForeignKey(BigData)
    region = models.ForeignKey(City)

    class Meta:
        abstract = True


class Item(BigDataItem):
    data = models.TextField()

    def __unicode__(self):
        return self.data


class Product(BigDataItem):
    class Meta:
        unique_together = ('title', 'category',)

    title = models.CharField(max_length=255)
    currency = models.CharField(max_length=10)
    price = models.CharField(max_length=140)
    category = models.CharField(max_length=140, blank=True)
    description = models.CharField(max_length=300, blank=True)
    thumbnail = models.CharField(max_length=140, blank=True)

    def __unicode__(self):
        return '%s - %s' % (self.title, self.price)


class Company(BigDataItem):
    class Meta:
        unique_together = ('name', 'phone', 'email', 'address')

    name = models.CharField(max_length=140)
    phone = models.CharField(max_length=140, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    address = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return '%s%s%s%s' % (
            self.name,
            ' %s' % self.phone if self.phone else '',
            ' %s' % self.email if self.email else '',
            ' %s' % self.address if self.address else '',
        )


class Person(BigDataItem):
    class Meta:
        # TODO: Check in ValidationPipeLine if instance ALSO have the same phone or address or email
        unique_together = ('name', 'last_name', )

    name = models.CharField(max_length=140)
    last_name = models.CharField(max_length=140, blank=True, null=True)
    phone = models.CharField(max_length=140, blank=True)
    email = models.EmailField(blank=True, null=True)
    address = models.CharField(max_length=255, blank=True)
    company = models.ForeignKey(Company, blank=True, null=True)

    def __unicode__(self):
        return '%s%s%s%s%s' % (
            '%s %s' % (self.name, self.last_name) if self.last_name else self.name,
            ' %s' % self.phone if self.phone else '',
            ' %s' % self.email if self.email else '',
            ' %s' % self.address if self.address else '',
            ' from %s' % self.company if self.company else ''
        )

"""
class GetData(models.Model):
    bigdata = models.ForeignKey(BigData)
    parameters = models.TextField()
"""
PRODUCT_MODEL = Product.__name__
COMPANY_MODEL = Company.__name__
PERSON_MODEL = Person.__name__
MODEL_TYPES = (
    PRODUCT_MODEL,
    COMPANY_MODEL,
    PERSON_MODEL,
)
