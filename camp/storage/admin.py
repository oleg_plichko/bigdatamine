from django.contrib import admin

from storage.models import Product, Company, Person, Item, Country, City


class ItemInline(admin.TabularInline):
    model = Item
    extra = 0


class ProductInline(admin.TabularInline):
    model = Product
    extra = 0


class CompanyInline(admin.TabularInline):
    model = Company
    extra = 0


class PersonInline(admin.TabularInline):
    model = Person
    extra = 0


admin.site.register(Country)
admin.site.register(City)
admin.site.register(Item)
admin.site.register(Product)
admin.site.register(Company)
admin.site.register(Person)
