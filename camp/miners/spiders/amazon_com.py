# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders.crawl import Rule

from miners.spiders.base import BigDataSpider, CrawlBigDataSpider


class AmazonSpider(CrawlBigDataSpider):
    #  TODO: fix radio to exctract name without http://
    name = 'amazon_com_librarian'
    allowed_domains = ["amazon.com"]
    SEARCH_BOOKS_URL = "http://www.amazon.com/s/ref=sr_nr_n_0?fst=as%3Aoff&rh=n%3A283155%2Ck%3APython&keywords={query}&ie=UTF8&qid=1437851929&rnid=2941120011"
    rules = (
        Rule(LinkExtractor(allow=('/dp/', )), callback='parse_item'),
    )

    def __init__(self, **kwargs):
        self.start_urls = [
            self.SEARCH_BOOKS_URL.format(query=kwargs.get('query', ''))
        ]
        super(AmazonSpider, self).__init__(**kwargs)

    def parse_item(self, response):
        # TODO: price_xpath = '//*[@id="newOfferAccordionRow"]//span[contains(@class, "header-price")]/text()'
        return {
            'bigdata_id': self.bigdata_id,
            'model': 'Product',
            'fields': {
                'title': response.xpath('//span[@id="productTitle"]/text()').extract()[0],
                'price': '29$',
                'thumbnail': response.xpath('//img[@id="mainImage" or @id="imgBlkFront"]/@src').extract()[0],
            },
        }


class DummySpider(BigDataSpider):
    #  TODO: fix radio to exctract name without http://
    name = 'amazon_com_dummy'

    def __init__(self, **kwargs):
        self.start_urls = [
            'http://localhost:8000/office',
        ]
        self.custom_settings = {'HTTPCACHE_POLICY': 'scrapy.extensions.httpcache.DummyPolicy'}
        super(DummySpider, self).__init__(**kwargs)

    def parse(self, response):
        return {
            'bigdata_id': self.bigdata_id,
            'model': 'Product',
            'fields': {
                'title': 'test',
                'price': '29$',
                'thumbnail': 'xnm23213.jpg',
            },
        }
