import inspect

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from equipment import palletlifter

from foremen.base import Foreman, Leader
from foremen.packtpub_com import Packtpub

scrapy_settings = get_project_settings()
import foremen


def register_foremen():
    #  TODO: find all foremen/ folder classes and. result[foreman_class.name] = foreman_class
    result = {}
    for module_name, module in inspect.getmembers(foremen):
        if inspect.ismodule(module):
            for class_name, _class in inspect.getmembers(module):
                if inspect.isclass(_class):
                    result[_class.name] = _class
    return result


foreman_by_name = {
    'foreman': Foreman,
    'leader': Leader,
    'packthub': Packtpub,
}

#foreman_by_name = register_foremen()

def receive_orders(bigdata):
    foreman_location = locate_foreman(bigdata)
    # TODO: remove ugly hack
    params = {}
    credentials = bigdata.team.miner.qualification.foreman.credentials
    if credentials is not None:
        params.update({'login': credentials.login, 'password': credentials.password})
    foreman = call_foreman(foreman_location, **params)
    miner_location = locate_miner(bigdata)
    instructions = {'bigdata_id': bigdata.pk}
    instructions.update(bigdata.team.instructions)
    instructions.update(foreman.instructions)
    return {'miner': miner_location, 'instructions': instructions}


def locate_miner(bigdata):
    miner_name = bigdata.team.miner.location.replace('->', '_')
    miner_name = miner_name.lower()
    return miner_name

def locate_foreman(bigdata):
    return bigdata.team.miner.qualification.foreman.location

def call_foreman(location, login=None, password=None):
    return foreman_by_name[location](login=login, password=password)

def call_miner(location, **instructions):
    process = CrawlerProcess(scrapy_settings)
    runner = process.crawl(location, **instructions)
    #runner.addBoth(lambda _: reactor.stop())
    process.start()

FOREMEN = foreman_by_name.keys()
