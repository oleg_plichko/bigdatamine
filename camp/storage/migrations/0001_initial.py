# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('name', models.CharField(max_length=140, serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('phone', models.CharField(max_length=140, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('address', models.CharField(max_length=255, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('name', models.CharField(max_length=140, serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('last_name', models.CharField(max_length=140, null=True, blank=True)),
                ('phone', models.CharField(max_length=140, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('address', models.CharField(max_length=255, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('currency', models.CharField(max_length=10)),
                ('price', models.CharField(max_length=140)),
                ('category', models.CharField(max_length=140, blank=True)),
                ('description', models.CharField(max_length=300, blank=True)),
                ('thumbnail', models.CharField(max_length=140, blank=True)),
            ],
        ),
    ]
