# -*- coding: utf-8 -*-
from scrapy.http.request import Request

from equipment import calculator
from miners.spiders.base import BigDataSpider, scrapes_bigdata_contract


class PacktpubSpider(BigDataSpider):
    name = 'packtpub_com_free_ebook_claimer'
    allowed_domains = ['packtpub.com']
    URL = 'https://www.packtpub.com{url}'
    start_urls = (
        'https://www.packtpub.com/packt/offers/free-learning',
    )

    def __init__(self, **kwargs):
        self.proxy = kwargs.get('proxy', [])
        super(PacktpubSpider, self).__init__(**kwargs)

    def parse(self, response):
        self.log('parse!')
        self.log(self.proxy)
        link = response.xpath('//a[contains(@class, "twelve-days-claim")]/@href').extract()[0]
        result = [
            Request(self.URL.format(url=link), cookies=self.cookies, callback=self.parse_items),
        ]
        return result

    def parse_items(self, response):
        self.log('parse_items!')
        self.log(response.url)
        for link in response.xpath('//div[contains(@class, "product-thumbnail")]/a/@href').extract():
            yield Request(self.URL.format(url=link), cookies=self.cookies, callback=self.parse_item)

    #@scrapes_bigdata_contract
    def parse_item(self, response):
        # TODO: figure out why contract is not working at all
        """


        @returns items 0 0
        @scrapes bigdata_id model fields
        """
        title = response.xpath('//*[@id="mobile-book-container"]/div[2]/div[1]/h1/text()')
        price = response.xpath('//*[@id="mobile-book-container"]/div[2]/div[5]/div[1]/div[1]/div[2]/text()').extract()[0].strip()
        price, currency = calculator.extract_price_and_currency(price)
        thumbnail = response.xpath('//*[@id="mobile-book-container"]/div[1]/div[1]/span/a/img/@src')
        description = response.css('#mobile-book-info > div.book-top-block-info-one-liner.cf::text')
        return {
            'bigdata_id': self.bigdata_id,
            'model': 'Product',
            #'region': 'Russia:Nizhniy Novgorod',
            'fields': {
                'title': title.extract()[0].strip(),
                'price': price,
                'currency': currency,
                'category': 'book',
                'thumbnail': thumbnail.extract()[0].strip(),
                'description': description.extract()[0].strip(),
            }
        }
