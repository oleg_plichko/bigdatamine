# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import re
import logging

from django.apps import apps
from django.db import IntegrityError

from storage.models import BigData, City

logger = logging.getLogger(__name__)


CITY_COUNTRY_REGEX = re.compile(r'(\w+):(\w+)')

class UpdateOrCreatePipeline(object):

    def process_item(self, item, spider):
        try:
            bigdata_id = item['bigdata_id']
            region = item.get('region', None)
            if region is None:
                region = 'Undefined:Undefined'
            city_country = CITY_COUNTRY_REGEX.search(region)
            country = city_country.group(1)
            city = city_country.group(2)
            region = City.objects.get(name=city)
            bigdata = BigData.objects.get(pk=bigdata_id)
            model = item['model']
            # TODO: replace with palletlifter.get_items
            app_label = 'storage'
            ModelClass = apps.get_model(app_label, item['model'])
            ModelClass.objects.update_or_create(bigdata=bigdata, region=region, **item['fields'])
            bigdata.update()
        except KeyError, e:
            logger.error('There is no field %s in item', e)
        except BigData.DoesNotExist, e:
            logger.error('BigData with %s pk does not exist', bigdata_id)
        except LookupError, e:
            logger.error('There is no model class %s in app %s', model, app_label)
        except IntegrityError, e:
            logger.error('Model exists')

