from django.contrib import admin

from office.models import MinerIdCard, Deposit, ForemanIdCard, Credentials

admin.site.register(MinerIdCard)
admin.site.register(Deposit)
admin.site.register(ForemanIdCard)
# TODO: add Credentials admin form with PasswordInput for password
admin.site.register(Credentials)
