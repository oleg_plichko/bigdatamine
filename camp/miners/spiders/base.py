import json

from scrapy import Spider, Request
from scrapy.http.response.html import HtmlResponse
from scrapy.spiders.crawl import CrawlSpider


# TODO: parse_item with this decorator returns nothig
def scrapes_bigdata_contract(func):
    def wrapped(self, *args, **kwargs):
        return func(self, *args, **kwargs)
    wrapped.__doc__ = """


    @scrapes bigdata_id model fields
    """
    return wrapped


# TODO: clean this fucking mess
class CrawlBigDataSpider(CrawlSpider):

    def __init__(self, **kwargs):
        self.bigdata_id = kwargs.get('bigdata_id', '-1')
        cookies = kwargs.get('cookies', {})
        if isinstance(cookies, str):
            self.cookies = self._load_cookies(cookies)
        self.cookies = cookies
        super(CrawlBigDataSpider, self).__init__()

    def parse(self, response):
        pass

    def parse_item(self, response):
        pass

    def make_requests_from_url(self, url):
        return Request(url, dont_filter=True, cookies=self.cookies)

    def _load_cookies(sell, path):
        with open(path) as data_file:
            return json.load(data_file)

    def _requests_to_follow(self, response):
        if not isinstance(response, HtmlResponse):
            return
        seen = set()
        for n, rule in enumerate(self._rules):
            links = [l for l in rule.link_extractor.extract_links(response) if l not in seen]
            if links and rule.process_links:
                links = rule.process_links(links)
            for link in links:
                seen.add(link)
                r = Request(url=link.url, callback=self._response_downloaded, cookies=self.cookies)
                r.meta.update(rule=n, link_text=link.text)
                yield rule.process_request(r)


class BigDataSpider(Spider):
    def __init__(self, **kwargs):
        self.bigdata_id = kwargs.get('bigdata_id', '-1')
        cookies = kwargs.get('cookies', {})
        if isinstance(cookies, str):
            self.cookies = self._load_cookies(cookies)
        self.cookies = cookies
        super(BigDataSpider, self).__init__()

    def parse(self, response):
        pass

    def parse_item(self, response):
        pass

    def _load_cookies(sell, path):
        with open(path) as data_file:
            return json.load(data_file)

    def make_requests_from_url(self, url):
        return Request(url, dont_filter=True, cookies=self.cookies)