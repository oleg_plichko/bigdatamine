# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('djcelery', '__first__'),
        ('office', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BigData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('custom_name', models.BooleanField(default=False)),
                ('time_modified', models.DateTimeField(auto_now=True)),
                ('time_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Request',
            fields=[
                ('name', models.CharField(max_length=140, serialize=False, primary_key=True)),
                ('filter', jsonfield.fields.JSONField(default=b'{}', blank=True)),
                ('bigdata', models.ForeignKey(to='work.BigData')),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('instructions', jsonfield.fields.JSONField(default={}, blank=True)),
                ('miner', models.ForeignKey(to='office.MinerIdCard')),
            ],
        ),
        migrations.CreateModel(
            name='UpdateTask',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bigdata', models.ForeignKey(to='work.BigData')),
                ('interval', models.ForeignKey(to='djcelery.IntervalSchedule')),
                ('task', models.ForeignKey(blank=True, editable=False, to='djcelery.PeriodicTask', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='request',
            name='team',
            field=models.ForeignKey(to='work.Team'),
        ),
        migrations.AddField(
            model_name='bigdata',
            name='team',
            field=models.ForeignKey(to='work.Team'),
        ),
    ]
