# -*- coding: utf-8 -*-
import re

PRICE = re.compile(r'\d+(?:\.\d{2})?')
USD = 'USD'
GPB = 'GPB'
RUB = 'RUB'
EUR = 'EUR'
CURRENCY_LITERAL = re.compile('%s|%s|%s|%s' % (USD, GPB, RUB, EUR))
# GPB_SYMBOL = u'£' # TODO: ascii codec can't encode character u'\xa3'
# CURRENCY_SYMBOL = re.compile(u'%s|\$|€' % GPB_SYMBOL)
CURRENCY_SYMBOL = re.compile(u'\$|€')
symbol_to_literal = {u'$': USD, u'€': EUR}

def extract_price_and_currency(price):
    # TODO: price = price.decode('utf8')?
    symbol = CURRENCY_SYMBOL.search(price)
    if symbol:
        currency = symbol_to_literal[symbol.group(0)]
    else:
        currency = CURRENCY_LITERAL.search(price)
    if currency:
        currency = currency.group(0)
    else:
        currency = u'£'
    return PRICE.search(price).group(0), currency

