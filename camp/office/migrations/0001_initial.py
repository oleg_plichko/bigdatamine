# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Credentials',
            fields=[
                ('id', models.CharField(max_length=240, serialize=False, editable=False, primary_key=True)),
                ('login', models.CharField(max_length=140)),
                ('password', models.CharField(max_length=140, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Deposit',
            fields=[
                ('id', models.CharField(max_length=240, serialize=False, editable=False, primary_key=True)),
                ('url', models.URLField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ForemanIdCard',
            fields=[
                ('id', models.CharField(max_length=240, serialize=False, editable=False, primary_key=True)),
                ('name', models.CharField(max_length=140, choices=[(b'foreman', b'foreman'), (b'packthub', b'packthub'), (b'leader', b'leader')])),
                ('credentials', models.ForeignKey(blank=True, to='office.Credentials', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='MinerIdCard',
            fields=[
                ('id', models.CharField(max_length=240, serialize=False, editable=False, primary_key=True)),
                ('name', models.CharField(max_length=140)),
                ('qualification', models.ForeignKey(to='office.Deposit')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='deposit',
            name='foreman',
            field=models.ForeignKey(to='office.ForemanIdCard'),
        ),
    ]
