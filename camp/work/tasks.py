from __future__ import absolute_import

from celery import shared_task
from celery.utils.log import get_task_logger
from scrapy.exceptions import ContractFail

from equipment import radio, palletlifter
from work.models import BigData, Request
from camp import exceptions

logger = get_task_logger(__name__)


@shared_task
def add(x, y):
    logger.info('Adding {0} + {1}'.format(x, y))
    return x + y


@shared_task
def execute_orders(orders):
    radio.call_miner(orders['miner'], **orders['instructions'])


@shared_task
def update_bigdata(pk):
    bigdata = BigData.objects.get(pk=pk)
    if not bigdata:
        raise exceptions.BigDataNotFound()
    bigdata.update()
    orders = radio.receive_orders(bigdata)
    instructions = orders['instructions']
    requests = Request.objects.filter(team=bigdata.team)
    for request in requests:
        results = instructions[request.name] = []
        for result in palletlifter.get_items('Item', request.bigdata, request.filter):
            results.append(result)
    radio.call_miner(orders['miner'], **instructions)


@shared_task
def save_file():
    f = open('test.txt', 'w')
    f.write('test, test')
    f.close()
