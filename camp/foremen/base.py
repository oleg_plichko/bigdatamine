from camp import exceptions


class Foreman(object):
    name = 'foreman'

    def __init__(self, **kwargs):
        self.login = kwargs.get('login', None)
        self.password = kwargs.get('password', None)

    @property
    def instructions(self):
        return {}


class Leader(Foreman):
    name = 'leader'

    def __init__(self, **kwargs):
        super(Leader, self).__init__(**kwargs)
        if self.login is None:
            raise exceptions.NoCredentials()

