import inspect

from django.db import models
from equipment import radio

from miners import spiders


class CompoundIdModel(models.Model):
    id = models.CharField(max_length=240, primary_key=True, editable=False)

    def clean(self):
        self.id = self.compound_id()

    class Meta:
        abstract = True


#  TODO: rename?
class Credentials(CompoundIdModel):
    login = models.CharField(max_length=140)
    password = models.CharField(max_length=140, blank=True)

    def __unicode__(self):
        return '%s:%s' % (self.login, self.password)

    def compound_id(self):
        return '%s:%s' % (self.login, self.password)


FOREMEN_CHOICE = [(key, key) for key in radio.FOREMEN]

class ForemanIdCard(CompoundIdModel):
    name = models.CharField(max_length=140, choices=FOREMEN_CHOICE)
    credentials = models.ForeignKey(Credentials, blank=True, null=True)

    def __unicode__(self):
        return '%s%s' % (self.name, '(%s)' % self.credentials if self.credentials else '')

    def compound_id(self):
        return '%s - %s' % (self.name, self.credentials)

    @property
    def location(self):
        return self.name


# TODO: add Qualification model with rights mb?
# TODO: get all spiders domains to make choise for url
class Deposit(CompoundIdModel):
    url = models.URLField()
    foreman = models.ForeignKey(ForemanIdCard)

    def __unicode__(self):
        return '%s@%s' %(self.foreman, self.url)

    def compound_id(self):
        return '%s@%s' %(self.foreman, self.url)


#  TODO: fix this to provide choices
def _get_miners_names():
    result = []
    for module_name, module in inspect.getmembers(spiders):
        if inspect.ismodule(module):
            for class_name, _class in inspect.getmembers(module):
                if inspect.isclass(_class):
                    result.append((_class.name[2:].upper(), _class.name))
    return result


#  TODO: miners? s?
MINERS_NAMES = _get_miners_names()


class MinerIdCard(CompoundIdModel):
    """
    :field id - path to miner script
    :field qualification - source domain

    """
    name = models.CharField(max_length=140, choices=MINERS_NAMES)
    qualification = models.ForeignKey(Deposit)

    def __unicode__(self):
        return '%s (%s)' %(self.name, self.qualification.url)

    def compound_id(self):
        return '%s--%s' %(self.name, self.qualification)

    @property
    def location(self):
        return '%s->%s' % (
            self.qualification.url.replace('http://', '').replace('.', '_'),
            self.name.replace(' ', '_').lower()
        )
