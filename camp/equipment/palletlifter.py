from django.apps import apps
from django.forms.models import model_to_dict

MODELS = (
    'Item',
    'Product',
    'Company',
    'Person',
)


def get_items(model, bigdata, filter):
    if model == '*':
        for model in MODELS:
            yield _get_items(model, bigdata, filter)
    else:
        yield _get_items(model, bigdata, filter)


def _get_items(model, bigdata, filter):
    app_label = 'storage'
    ModelClass = apps.get_model(app_label, model)
    items = ModelClass.objects.filter(bigdata=bigdata)
    results = []
    for item in items:
        item = model_to_dict(item)
        result = {}
        for field in item:
            if field in filter['fields']:
                result[field] = item[field]
        results.append(result)
    return results
