from django.shortcuts import redirect

from work.tasks import update_bigdata


# Create your views here.
def update(request, pk):
    #  TODO: clean bigdta items to prevent doubles or update doubles. related to validation pipiline
    #  TODO: replace with execute_orders task
    update_bigdata.delay(pk)
    return redirect('/work/bigdata/')

"""
def _recieve_orders(pk):
    bigdata = BigData.objects.get(pk=pk)
    if not bigdata:
        raise exceptions.BigDataNotFound()
    bigdata.update()
    orders = radio.receive_orders(bigdata)
"""