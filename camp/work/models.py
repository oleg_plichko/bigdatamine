from datetime import datetime
import json

from django.contrib.humanize.templatetags.humanize import naturaltime
from django.db import models
from djcelery.models import IntervalSchedule, PeriodicTask
from jsonfield import JSONField


from office.models import MinerIdCard


class Team(models.Model):
    miner = models.ForeignKey(MinerIdCard)
    instructions = JSONField(blank=True, default={})

    def deposit(self):
        return self.miner.qualification.url

    def __unicode__(self):
        return '%s with %s' % (self.miner, self.instructions)


class BigData(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    custom_name = models.BooleanField(default=False)
    team = models.ForeignKey(Team)
    time_modified = models.DateTimeField(auto_now=True, blank=True)
    time_created = models.DateTimeField(auto_now_add=True, blank=True)

    def generate_name(self):
        """
        products = Product.objects.filter(bigdata=self).count()
        companies = Company.objects.filter(bigdata=self).count()
        persons = Person.objects.filter(bigdata=self).count()
        result = '%d Product ' % products if products > 0 else ''
        result += ' %d Company' % products if companies > 0 else ''
        result += ' %d Person' % products if persons > 0 else ''
        """
        result = 'items from %s ' % self.team.miner.qualification.url
        for key, value in self.team.instructions.iteritems():
            result += ('%s is %s ' % (key, value))
        return result

    #  TODO: mess with the get_name and name field it self
    def get_name(self):
        if not self.custom_name:
            return self.generate_name()
        return self.name

    def clean(self):
        if not self.custom_name or not self.name:
            self.name = self.generate_name()

    def update(self):
        self.time_modified = datetime.now()
        if not self.custom_name:
            self.name = self.generate_name()
        self.save()

    def updated(self):
        if self.time_modified == self.time_created:
            return 'never'
        else:
            return naturaltime(self.time_modified).encode('utf-8')

    def created(self):
        return naturaltime(self.time_created).encode('utf-8')

    def __unicode__(self):
        # TODO: change
        return '%s with %s' % (self.team.miner, self.team.instructions)


class Request(models.Model):
    name = models.CharField(primary_key=True, max_length=140)
    bigdata = models.ForeignKey(BigData)
    team = models.ForeignKey(Team)
    filter = JSONField(blank=True, default='{}')


from equipment import radio

class UpdateTask(models.Model):
    bigdata = models.ForeignKey(BigData)
    interval = models.ForeignKey(IntervalSchedule)
    task = models.ForeignKey(PeriodicTask, blank=True, null=True, editable=False)

    def clean(self):
        if self.task:
            self.task.delete()
        task = PeriodicTask(
            name=self.bigdata.__unicode__(),
            interval=self.interval,
            task='work.tasks.execute_orders',
            kwargs=json.dumps(radio.receive_orders(self.bigdata))
        )
        task.save()
        self.task = task

    def delete(self, using=None):
        self.task.delete()
        super(UpdateTask, self).delete(using=using)

    def __unicode__(self):
        return self.task.__str__()
