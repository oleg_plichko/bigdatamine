class NoCredentials(Exception):
    def __init__(self, message='No credentials were provided'):
        super(NoCredentials, self).__init__(message)

class BigDataNotFound(Exception):
    def __init__(self, message='No credentials were provided'):
        super(NoCredentials, self).__init__(message)
