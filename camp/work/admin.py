from django.contrib import admin
from django.shortcuts import redirect

from storage.admin import ProductInline, CompanyInline, PersonInline, ItemInline
from work.models import BigData, UpdateTask, Request, Team


def bulk_update(modeladmin, request, queryset):
    bigdata = BigData.objects.all()[0]
    return redirect('/work/update/%s' % bigdata.pk)
    modeladmin.message_user(request,
                            ('Successfully updated price for %d rows') % (queryset.count(),),
                            messages.SUCCESS
    )

bulk_update.short_description = 'Update items'


def update(obj):
    return '<a href="/work/obtain/%s">%s</a>' % (obj.pk, 'update')

update.allow_tags = True
update.short_description = 'Update selected items.'


def new_bulk_delete(modeladmin, request, queryset):
    n = 0
    for obj in queryset:
        obj.delete()
        n += 1
    # TODO: modeladmin.message_user(request, 'Successfully deleted %d items.' % n, messages.SUCCESS)
    return None


class NoBulkDeleteAdmin(admin.ModelAdmin):
    def get_actions(self, request):
        actions = super(NoBulkDeleteAdmin, self).get_actions(request)
        del actions['delete_selected']
        actions['delete_selected'] = (
            new_bulk_delete,
            'delete_selected',
            'Delete selected items'
        )
        return actions


class RequestInline(admin.TabularInline):
    model = Request
    extra = 0


@admin.register(BigData)
class BigDataAdmin(admin.ModelAdmin):
    # TODO: wider TextInput for name field
    list_display = ('get_name', 'updated', 'created', update)
    ordering = ['time_created']
    actions = [admin.actions.delete_selected, bulk_update]
    inlines = [ItemInline, ProductInline, CompanyInline, PersonInline]
    fieldsets = (
        (None, {
            'fields': (
                ('name', 'custom_name'),
                'team',
            )
        }),
    )
    """
    TODO:

        ('Advanced options', {
            'classes': ('collapse',),
            'fields': ('time_created', 'time_modified')
        }),
    """

@admin.register(UpdateTask)
class UpdateTaskAdmin(NoBulkDeleteAdmin):
    pass


class RequestInline(admin.TabularInline):
    model = Request
    extra = 0


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    inlines = [RequestInline]