from django.conf.urls import url

from work import views

urlpatterns = [
    url(r'^obtain/(?P<pk>\w+)$', views.update, name='update'),
]
