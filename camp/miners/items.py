# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy_djangoitem import DjangoItem

from storage.models import Product, Company, Person


class ProductItem(DjangoItem):
    django_model = Product


class CompanyItem(DjangoItem):
    django_model = Company


class PersonItem(DjangoItem):
    django_model = Person
