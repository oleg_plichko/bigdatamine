import requests

from foremen.base import Leader


class Packtpub(Leader):
    name = 'packthub'

    @property
    def instructions(self):
        data = {
            'email': self.login,
            'password': self.password,
            'op': 'Login',
            'form_build_id': 'form-bc5c15f1eb560189454ff6fa0537512e',
            'form_id': 'packt_user_login_form',
        }
        session = requests.session()
        response = session.post('https://www.packtpub.com', data=data)
        cookies = requests.utils.dict_from_cookiejar(session.cookies)
        assert response.status_code == 200
        # TODO: figureout how to get regular dict for header cuz response.headers is not json serializable for celery task
        result = {'headers': {}, 'cookies': cookies}
        result.update(super(Packtpub, self).instructions)
        return result

