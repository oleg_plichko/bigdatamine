# -*- coding: utf-8 -*-
import urllib
import re
from io import BytesIO
import urllib2
import socket

from scrapy.http.request import Request
from lxml import etree

from miners.spiders.base import BigDataSpider


class HideMyAssSpider(BigDataSpider):
    name = 'hidemyass_com_free_proxy_miner'
    allowed_domains = ['hidemyass.com']

    def __init__(self, **kwargs):
        super(HideMyAssSpider, self).__init__(**kwargs)

    def start_requests(self):
        # TODO: replace with kwargs from __init__. move to init possible.
        url = 'http://proxylist.hidemyass.com'
        params = {
            "ac": "on",
            "c%5B%5D": [
                "China",
                "United+States",
                "Russian+Federation",
                "Venezuela",
                "Indonesia",
                "Brazil",
                "India",
                "Netherlands",
                "Taiwan",
                "Taiwan",
                "Thailand",
                "Czech+Republic",
                "Korea%2C+Republic+of",
                "Bangladesh",
                "Germany",
                "France",
                "United+Kingdom",
                "Chile",
                "Serbia",
                "Nepal",
                "Hong+Kong",
                "Viet+Nam",
                "Argentina",
                "Austria",
                "Europe",
                "Japan",
                "Malaysia",
                "Poland",
                "Iraq",
                "Spain",
                "South+Africa",
                "Kazakhstan",
                "Slovakia",
                "Ukraine",
                "Bulgaria",
                "Mexico",
                "Moldova%2C+Republic+of",
                "Romania",
                "Pakistan",
                "Greece",
                "Italy",
                "Israel",
                "Georgia",
                "Azerbaijan",
                "Tanzania%2C+United+Republic+of",
                "Cameroon",
                "Belarus",
                "Uzbekistan",
                "Macedonia",
                "Zambia",
                "Iran",
                "Colombia",
                "Belgium",
                "Mozambique",
                "Mongolia",
                "Portugal",
                "Philippines",
                "Panama",
                "Latvia",
                "Switzerland",
                "Kenya",
                "Singapore",
                "United+Arab+Emirates",
                "Sweden",
                "Denmark",
                "Nigeria",
                "Bolivia",
                "Netherlands+Antilles"
            ],
            "p": "8080",
            "pr%5B%5D": "0",
            "a%5B%5D": [
                "0",
                "1",
                "2",
                "3",
                "4"
            ],
            "pl": "on",
            "sp%5B%5D": [
                "2",
                "3"
            ],
            "ct%5B%5D": [
                "2",
                "3"
            ],
            "s": "3",
            "o": "0",
            "pp": "3",
            "sortBy": "date"
        }
        data = urllib.urlencode(params)
        return [Request(url, body=data, method='POST', cookies=self.cookies)]

    def parse(self, response):
        for row in response.xpath('//td[@class="leftborder timestamp"]/parent::tr'):
            masking_styles = row.xpath('td[2]/span/style/text()').extract()[0]
            masked_ip_html = row.xpath('td[2]').extract()[0]
            data = '%s:%d' % (
                self.unmask_ip(masking_styles, masked_ip_html),
                int(row.xpath('td[3]/text()').extract()[0])
            )
            urllib2.install_opener(
                urllib2.build_opener(
                    urllib2.ProxyHandler({'http': data})
                )
            )
            try:
                code = urllib2.urlopen('http://google.com', timeout=1).getcode()
            except Exception:
                continue
            if code == 200:
                yield {
                    'bigdata_id': self.bigdata_id,
                    #'region': 'Russia:Nizhniy Novgorod',
                    'model': 'Item',
                    'fields': {
                        'data': data
                    }
                }

    def unmask_ip(self, masking_styles, masked_ip_html):
        html = masked_ip_html
        for ms in masking_styles.strip().split('\n'):
            style_name, replace_style = ms.strip('.} ').split('{')
            html = html.replace(style_name, replace_style)
        html = re.sub(r"(?P<tag></.+?>)(?P<value>[\d\.]+)", r"\1<span>\2</span>", html)
        context = etree.iterparse(BytesIO(html.encode('utf-8')))
        for action, elem in context:
            if elem.tag == 'style':
                elem.clear()
                continue
            if 'style' in elem.attrib or 'class' in elem.attrib:
                attr = elem.attrib.get('style') or elem.attrib.get('class')
                if 'none' in attr and action == 'end':
                    elem.clear()

        html = etree.tostring(context.root, method="html")
        html = re.sub(r'<[^>]*?>', '', html)
        html = re.sub(r'\s+', '' , html)
        return html
